import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Item{
    parent.background: Rectangle{
        width: parent.width
        height: parent.height
        color:"black"

        border.width: 3
        border.color: "white"

    }
    contentItem: Text{

        color:"white"
        text: parent.text

    }
}
