import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.15

Item {
    id: customSwitchModAuto
    property string switchMod: "Автоматический режим"
    property int switchTrigger1: 0

    Text{
        x:100
        id: label_type_of_work

        font.pointSize: 10
        color: "white"
        text:customSwitchModAuto.switchMod

    }
    Switch {
        id: switch1
        anchors.verticalCenter:label_type_of_work.verticalCenter
        Material.accent: Material.Green
        onToggled:
        {
            if(checked)
            {

                label_type_of_work.color = "green"
                classA.workModAutoOn();
                classA.pumpIncOff()
                classA.pumpDecOff()
                // второй свич

                label_type_of_hand.color = "white"
                classA.workModHandOff();

                //switch2.checked = false
                window.switch2 = false


            }
            else
            {

                label_type_of_work.color = "white"
                classA.workModAutoOff();


            }

        }
    }
}
