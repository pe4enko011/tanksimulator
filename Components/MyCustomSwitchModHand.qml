import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.15


Item {
    id: customSwitchModHand
    property string switchMod: "Ручной режим"
    property int switchTrigger1: 0
    Text{
        x:100
        id: label_type_of_hand

        font.pointSize: 10
        color: "white"
        text:customSwitchModHand.switchMod

    }
    Switch {
        id: switch2
        anchors.verticalCenter:label_type_of_work.verticalCenter
        Material.accent: Material.Green
        onToggled:
        {
            if(checked)
            {

                label_type_of_hand.color = "green"
                classA.workModHandOn();
                classA.pumpIncOff()
                classA.pumpDecOff()

                // второй свич
                label_type_of_work.color = "white"
                classA.workModAutoOff()

                window.switch1.checked = false



            }
            else
            {
                label_type_of_hand.color = "white"
                classA.workModHandOff();



            }

        }
    }
}
