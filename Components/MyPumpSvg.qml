import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12


Item{
    id:pumpSvg

    Image{
        source: "src/svg/pump.svg"
        visible:  window.pump ? false : true

    }

    Image{
        source: "src/svg/red_p.svg"
        visible:  window.pump ? true : false

    }

    Label{
        text: "Накачка"
        color:"green"
        anchors.top: parent.top
        anchors.topMargin: 45
        visible: window.pump
    }
}
