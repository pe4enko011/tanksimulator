import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Item{
    id:tank


    Image {
        id: tank_svg
        source: "src/svg/tank.svg"
        x:150
        y:80
        clip:true


        Rectangle{
            id: tank_top

            width: 38
            height:  window.tank * 0.188;   //
            anchors.left: tank_svg.left
            anchors.bottom: tank_svg.bottom
            anchors.leftMargin : 67
            anchors.bottomMargin : 11

            color: "#008000"
        }

        Rectangle{
            id: waterLevel

            width: 38
            height:  2   //
            anchors.left: tank_svg.left
            anchors.bottom: tank_svg.bottom
            anchors.leftMargin : 67
            anchors.bottomMargin : window.tank * 0.188 + 9

            color: "white"
        }
    }

    function pupmOn( per){
        if(per === 0){
            color = "gray"
        }else{
            color = "red"
        }
    }


}
