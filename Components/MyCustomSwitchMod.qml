import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.15

Item {
    id: customSwitchMod
    property string switchMod: "Автоматический режим"
    property int switchTrigger1: 0
    Text{
        x:100
        id: label_type_of_work

        font.pointSize: 10
        color: "white"
        text:customSwitchMod.switchMod

    }
    Switch {
        id: switch1      
        anchors.verticalCenter:label_type_of_work.verticalCenter
        Material.accent: Material.Green
        onToggled:
        {
            if(checked)
            {
                label_type_of_work.color = "green"


            }
            else
            {
                label_type_of_work.color = "white"


            }

        }
    }
}
