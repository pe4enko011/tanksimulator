import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12


Item{
    id:customButtonPumpOnPumpOff
    x: 300
    y:350



    Button {
        id: downButton
        //anchors.centerIn: parent
        text: customButtonPumpOnPumpOff.textButton

        background: Rectangle{
            width: parent.width
            height: parent.height
            color:"black"

            border.width: 3
            border.color: "white"

        }
        contentItem: Text{

            color:"white"
            text: parent.text

        }

        onClicked:{

            if(customButtonPumpOnPumpOff.pump === 1){
                panel.startPumpInProcess()
            }else if(customButtonPumpOnPumpOff.pump === 0 ){
                panel.stopProcessPumpOnInHandMod()
            }else if(customButtonPumpOnPumpOff.pump === 3){
                panel.startPumpOffProcess()
            }else if(customButtonPumpOnPumpOff.pump === 2){
                panel.stopProcessPumpOffInHandMod()
            }

             Qalarm.compareHight(window.tank)
        }


    }
}
