import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Item{
    id:pumpoffSvg

    Image{
        source: "src/svg/pump.svg"
        visible:  window.pumpOff ? false : true

    }

    Image{
        source: "src/svg/red_p.svg"
        visible:  window.pumpOff ? true : false

    }

    Label{
        text: "Откачка"
        color:"green"
        anchors.top: parent.top
        anchors.topMargin: 45
        visible: window.pumpOff
    }
}
