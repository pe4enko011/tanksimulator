import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12

Item{
    id:alarmSignals
    property bool l: false
    property bool cl: false
    property bool h: false
    property bool ch: false

    Image{
        id: labeLow
        source: "src/svg/yellow_low.svg"
        visible: alarmSignals.l
    }
    Label{
        id: labeCritLow
        text: "CriticalLow"
        visible: alarmSignals.cl
        color: "red"
        font.pixelSize: 22
    }

    Label{
        id: labeHight
        text: "Hight"
        visible: alarmSignals.h
        color: "yellow"
        font.pixelSize: 22
    }
    Label{
        id: labeCriticaHight
        text: "CriticalHight"
        visible: alarmSignals.ch
        color: "red"
        font.pixelSize: 22
    }

}
