import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.15

Button{

    id: startProcessHand
    text:"STOP PROCESS"
    anchors.left:startButton.right
    anchors.leftMargin: 50

    visible: workModAvto ? true : false

    background: Rectangle{
        width: parent.width
        height: parent.height
        color:"black"

        border.width: 3
        border.color: "white"

    }
    contentItem: Text{

        color:"white"
        text: parent.text

    }
    onClicked: {
        classA.pumpIncOff()
        classA.pumpDecOff()


    }
}
