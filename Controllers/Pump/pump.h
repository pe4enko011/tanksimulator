#ifndef PUMP_H
#define PUMP_H

#include <QObject>
#include <QString>
#include <iostream>

/*! \brief Класс модели насоса */

class PumpModel{
public:
    /*! \brief Функия возвращает значение скорости наполнения/откачки насоса */
    int getPumpValue();
    /*! \brief Функия возвращает стичное значение скорости наполнения/откачки насоса */
    int getpumpStaticValue();
    /*! \brief Функия устанавливает значение скорости наполнения/откачки насоса */
    void setPumpValue(int newIspumpOn);
    /*! \brief Функия устанавливает стичное значение скорости наполнения/откачки насоса */
    void setpumpStaticValue(int pumpValueFromConstructor);

    int pump;

private:
    int pumpStaticState;
    int pumpChangebleState;

};

/*! \brief Класс насоса */
class Pump {
public:
    Pump(int typePump){
       pumpType = typePump;
       pumpModel.setpumpStaticValue(pumpType);
       pumpModel.setPumpValue(pumpType);
    }

    /*! \brief Функия установливает скорость наполнения/откачки насоса */
    int setSpeedOfFillingTank(){return pumpModel.getPumpValue();}

    /*! \brief Функия возвращает значение скорости наполнения/откачки насоса */
    int getPumpStaticValue(){return pumpModel.getpumpStaticValue();}
    /*! \brief Функия устанавливает значение скорости наполнения насоса */
    void pumpOn(){pumpModel.setPumpValue(pumpModel.getpumpStaticValue());}
    /*! \brief Функия устанавливает нулевое значение скорости наполнения насоса   */
    void pumpOff(){pumpModel.setPumpValue(0);}

    /*! \brief Функия проверяет скорость наполения насоса и возвращает true, если насос накачивающий */
    bool isMaxValueReached(){
        if(pumpModel.getPumpValue() > 0){
            return true;
        }
        return false;
    }

    /*! \brief Функия проверяет скорость наполения насоса и возвращает true, если насос накачивающий */
    bool isMinValueReached(){
        if(pumpModel.getPumpValue() < 0 ){
            return true;
        }
        return false;
    }

private:
    PumpModel pumpModel;
    int pumpType;
};

#endif
