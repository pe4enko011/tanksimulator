#include "tankconteroller.h"
#include <iostream>


void TankController::setPumpIteration(int PumpTypeState){
        tankModel.setVolumeIncDecState(PumpTypeState);

}

int TankController::getPumpVolume(){
    return tankModel.getTankVolumeState();
}


void TankController::changeTankLevel(){
     tankModel.setTankVolumeState(tankModel.getTankVolumeState() + tankModel.getVolumeIncDecState());
}


int TankModel::getTankVolumeState()
{
    return tankVolumeState;
}

void TankModel::setTankVolumeState(int newTankLevelState)
{
    tankVolumeState = newTankLevelState;
}


int TankModel::getVolumeIncDecState(){
    return volumeIncDecState;
}

void TankModel::setVolumeIncDecState(int volumeIncDec){
    volumeIncDecState = volumeIncDec;
}
