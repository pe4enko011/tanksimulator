#ifndef TANKCONTEROLLER_H
#define TANKCONTEROLLER_H


#include <QObject>
#include <QString>

/*! \brief Класс модель бака.  */
class TankModel{
public:
     /*! \brief Функция отвечает за получение значения стейта уровня бака */
    int getTankVolumeState() ;
    /*! \brief Функция отвечает за установку значения стейта уровня бака */
    void setTankVolumeState(int volumeIncDec);
    /*! \brief Функция отвечает за получение значения стейта кванта наполнения/откачки  */
    int getVolumeIncDecState();
    /*! \brief Функция отвечает за установку значения стейта кванта наполнения/откачки */
    void setVolumeIncDecState(int volumeDec);

private:
    /*! \brief Значение уровня бака */
    int tankVolumeState = 0;
    /*! \brief Значение кванта наполнения/откачки  */
    int volumeIncDecState = 0;

};

/*! \brief Класс контроллера бака.   */
class TankController
{

public:
      /*! \brief Функция отвечают за получение значения уровня бака */
     int getPumpVolume();
      /*! \brief Функция отвечают за установку значения уровня бака */
     void setPumpIteration(int val);
      /*! \brief Функция отвечают за изменения значения уровня бака */
     void changeTankLevel();

private:

    TankModel tankModel;
};


#endif // TANKCONTEROLLER_H
