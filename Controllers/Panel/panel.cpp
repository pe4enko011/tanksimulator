#include "panel.h"
#include "Controllers/TankController/tankconteroller.h"
#include "Controllers/Pump/pump.h"

#include <iostream>



Panel::Panel(QObject *parent) :
    QObject(parent)
{
    connect(&timerOfFillingKvant, SIGNAL(timeout()), this, SLOT(setPumpVolume()));
    timerOfFillingKvant.start(100);
}

// Автомаический процесс
void Panel::setAutoMod(){  // Установка автоматического режима
    AutoOrManualMod = true;
}

void Panel::chooseIncOrDec(){  // Смена режима работы насосов при достижении предельного значания
    if(tankVolumeController.getPumpVolume() >= 1000) {
        pumpDec.pumpOn();
        pumpInc.pumpOff();
    }
    if(tankVolumeController.getPumpVolume() <= 0){
        pumpDec.pumpOff();
        pumpInc.pumpOn();
    }
}

void Panel::stopIncOrDecPumpsInHamdMod(){ // Отсановка насосов в ручном режиме при достижении предельного значениия
    if(tankVolumeController.getPumpVolume() >= 1000) {
        pumpDec.pumpOff();
        pumpInc.pumpOff();
    }
    if(tankVolumeController.getPumpVolume() <= 0){
        pumpDec.pumpOff();
        pumpInc.pumpOff();
    }
}


void Panel::switchOffAllPumps(){   // Выключение всех насосов
    pumpDec.pumpOff();
    pumpInc.pumpOff();

    updatePumpDecStatus();
    updatePumpIncStatus();
}

void Panel::startProcess(){   // Запск в автоматическом режиме
    isStartProcessOn = true;
    pumpDec.pumpOff();
    pumpInc.pumpOn();

}

void Panel::startPumpInProcess(){  // Запуск накачивающего насоса в ручном режиме
    isStartManualPunpOn = true;
    isStartManualPunpOff = false;
   // pumpDec.pumpOff();
    pumpInc.pumpOn();
    updatePumpDecStatus();
    updatePumpIncStatus();
}

void Panel::startPumpOffProcess(){  // Запуск откачивающего насоса в ручном режиме

    isStartManualPunpOff = true;
    isStartManualPunpOn = false;
    //pumpInc.pumpOff();
    pumpDec.pumpOn();

    updatePumpDecStatus();
    updatePumpIncStatus();
}

void Panel::stopProcess(){   // Остановка проессв в автоматическом режиме
    isStartProcessOn = false;
    isStartManualPunpOn = false;
    isPumpDecOn = false;
    isPumpIncOn = false;
    pumpInc.pumpOff();
    pumpDec.pumpOff();

    updatePumpDecStatus();
    updatePumpIncStatus();

}

void Panel::stopProcessPumpOnInHandMod(){ // Остановка накачивающего насосса в ручном рержиме
    isStartManualPunpOn = false;
    isPumpIncOn = false;
    pumpInc.pumpOff();
    updatePumpIncStatus();
    updatePumpDecStatus();
}

void Panel::stopProcessPumpOffInHandMod(){ // Остановка откачивающего насосса в ручном рержиме

    isStartManualPunpOff= false;
    isPumpDecOn = false;
    pumpDec.pumpOff();
    updatePumpDecStatus();
    updatePumpIncStatus();
}

void Panel::setPumpVolume(){

    if(isStartProcessOn){
        chooseIncOrDec();
        tankVolumeController.setPumpIteration(pumpInc.setSpeedOfFillingTank() + pumpDec.setSpeedOfFillingTank());  // setSpeedOfFillingTank   // getValue
        tankVolumeController.changeTankLevel(); // updateLevel
        isPumpIncOn = pumpInc.isMaxValueReached();
        isPumpDecOn = pumpDec.isMinValueReached();
    }else if(isStartManualPunpOn){
        startPumpIn();
        isPumpIncOn = pumpInc.isMaxValueReached();
        isPumpDecOn = pumpDec.isMinValueReached();
        stopIncOrDecPumpsInHamdMod();
    }else if(isStartManualPunpOff){
        startPumpOff();
        isPumpIncOn = pumpInc.isMaxValueReached();
        isPumpDecOn = pumpDec.isMinValueReached();
        stopIncOrDecPumpsInHamdMod();
    }
    updatePumpDecStatus();
    updatePumpIncStatus();
    updateLevel();
}
void  Panel::workModAutoOn(){isWorkAuto = true; isWorkManual = false; workAutoChanged();}
void  Panel::workModAutoOff(){isWorkAuto = false;  workAutoChanged();}
void  Panel::workModHandOn(){isWorkManual = true; workManualChanged(); }
void  Panel::workModHandOff(){ isWorkManual = false; workManualChanged();}

// Ручной процесс

void Panel::startPumpIn(){
    tankVolumeController.setPumpIteration(pumpInc.setSpeedOfFillingTank() + pumpDec.setSpeedOfFillingTank());
    tankVolumeController.changeTankLevel();


}
void Panel::startPumpOff(){
    tankVolumeController.setPumpIteration(pumpInc.setSpeedOfFillingTank() + pumpDec.setSpeedOfFillingTank());
    tankVolumeController.changeTankLevel();
}

void Panel::stopPumpIn(){

}

void Panel::startPumpOut(){
    tankVolumeController.setPumpIteration(pumpDec.getPumpStaticValue());
    tankVolumeController.changeTankLevel();

    isPumpIncOn = false;
    isPumpDecOn = true;

    updatePumpDecStatus();
    updatePumpIncStatus();
    updateLevel();
}

void Panel::stopPumpOut(){

}
void Panel::setHandMod(){

};
