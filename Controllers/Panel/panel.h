#ifndef PANEL_H
#define PANEL_H

#include <QObject>
#include <QString>
#include "Controllers/Pump/pump.h"
#include "Controllers/TankController/tankconteroller.h"
#include <QTime>
#include <QTimer>

/*!
    \brief Включат себя объекты бака и насосов, перерабатывает данные из этих объектов и
    передает информацию в графический интерфейс.
*/
class Panel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool pumpDecIsOn READ pumpDecIsOn  NOTIFY updatePumpDecStatus)
    Q_PROPERTY(bool pumpIncIsOn READ pumpIncIsOn  NOTIFY updatePumpIncStatus)
    Q_PROPERTY(int tankVolume READ tankVolume  NOTIFY updateLevel)


    // Установка рабочего/автоматического режима
    Q_PROPERTY(bool workAuto READ workAuto NOTIFY workAutoChanged)
    Q_PROPERTY(bool workHand READ workHand NOTIFY workManualChanged)

public:
    explicit Panel(QObject *parent = nullptr);
    /*!  \brief Передает данные об откачивающем насосе в графический итерфейс */
    bool pumpDecIsOn(){
        return isPumpDecOn;
    }
     /*!  \brief Передает данные о накачивающем насосе в графический итерфейс */
    bool pumpIncIsOn(){
        return isPumpIncOn;
    }
    /*!  \brief Передает данные о баке в графический итерфейс */
    int tankVolume(){
        return tankVolumeController.getPumpVolume();
    }

    /*!  \brief Передает данные об автоматическом режиме */
    bool workAuto(){return isWorkAuto;}
    /*!  \brief Передает данные оручном режиме */
    bool workHand(){return isWorkManual;}

signals:
        /*!  \brief Сигнализирует об изменении состояния откачавающего насоса */
        void updatePumpDecStatus();

        /*! \brief Сигнализирует об изменении состояния накачивающего насоса */
        void updatePumpIncStatus();

        /*! \brief Сигнализирует об изменении уровня насоса */
        void updateLevel();

        /*! \brief Сигнализирует об изменении автоматитческого режима */
        void workAutoChanged();

        /*! \brief Сигнализирует об изменении ручного режима */
        void workManualChanged();

public slots:

        /*! \brief Установка автоматического режима */
       void setAutoMod();

       /*! \brief Запуск проесса в автоматическом режиме  */
       void startProcess();

       /*! \brief Остановка проесса в автоматическом режиме */
       void stopProcess();

       /*! Установка ручного режима */
       void setHandMod();

       /*! \brief Запуск накачивающего насоса в ручном режиме */
       void startPumpIn();

       /*! \brief Установка накачивающего насоса в ручном режиме */
       void stopPumpIn();

       /*! \brief Запуск откачивающего насоса в ручном режиме */
       void startPumpOut();

       /*! \brief Остановка откачивающего насоса в ручном режимеn */
       void stopPumpOut();

       /*! \brief Запуск накачивающего насоса в ручном режиме */
       void startPumpInProcess();

       /*! \brief Установка уровня в баке */
       void setPumpVolume();

       /*! \brief Смена режима работы насосов при достижении предельного значания */
       void chooseIncOrDec();

       /*! \brief Включение автоматического режима */
       void workModAutoOn();

       /*! \brief Выключение ручного режима */
       void workModHandOn();

       /*! \brief Выключение автоматического режима */
       void workModAutoOff();

       /*! \brief Выключение ручного режима  */
       void workModHandOff();

       /*! \brief Выключение всех насосов */
       void switchOffAllPumps();

        /*! \brief  Отсановка насосов в ручном режиме при достижении предельного значениия */
        void stopIncOrDecPumpsInHamdMod();

        /*! \brief  Остановка накачивающего насосса в ручном рержиме */
        void stopProcessPumpOnInHandMod();

        /*! \brief Остановка откачивающего насосса в ручном рержиме */
        void stopProcessPumpOffInHandMod();

        /*! \brief Запуск откачивающего насоса в ручном режиме */
        void startPumpOffProcess();

        /*! \brief Запуск откачивающего насоса в ручном режиме */
        void startPumpOff();

private:

    /*! \brief Если true,  то автоматический режим, false - ручной режим*/
    bool AutoOrManualMod = true;

    /*! \brief Переменная отвечает за запуск проесса в автоматическом режиме*/
    bool isStartProcessOn = false;

     /*! \brief Переменная отвечает за запуск проесса накачки в ручном режиме*/
    bool isStartManualPunpOn = false;
     /*! \brief Переменная отвечает за запуск проесса откачки в ручном режиме*/
    bool isStartManualPunpOff = false;

     /*! \brief Вкл/Выкл  автоматический режим */
    bool isWorkAuto = true;
    /*! \brief Вкл/Выкл  ручной режим */
    bool isWorkManual = false;

    /*! \brief Вкл/Выкл  накачивающий насос */
    bool isPumpIncOn = false;

    /*! \brief Вкл/Выкл  откачивающий насос */
    bool isPumpDecOn = false;

    /*! \brief Контроллер бака */
    TankController tankVolumeController;
    /*! \brief Контроллер накачивающего насоса */
    Pump pumpInc{10};
    /*! \brief Контроллер откачивающего насоса */
    Pump pumpDec{-10};
    /*! \brief Таймер отвечающий за квант наполнения насоса */
    QTimer timerOfFillingKvant;  //
};

#endif // PANEL_H
