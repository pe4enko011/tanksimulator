#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QString>
#include <Controllers/TankController/tankconteroller.h>
#include <Controllers/Pump/pump.h>
#include <Controllers/Panel/panel.h>

//#include <Controllers/TankPumpsInterface/tankpumpsinterface.h>

//#include "ControlProcess.h"
//#include <UHelpers.h>
//#include <UniSetActivator.h>
#include <iostream>
using namespace std;
int main(int argc, char *argv[])
{


//    try {
//      auto conf = uniset_init(argc, argv);

//      auto act = UniSetActivator::Instance();

//      auto cp1 =
//          uniset::make_object<ControlProcess>("ControlProcess", "ControlProcess");
//      act->add(cp1);

//      SystemMessage sm(SystemMessage::StartUp);
//      act->broadcast(sm.transport_msg());

//      cout << "\n\n\n";
//      cout << "(main): -------------- ControlProcess START "
//              "-------------------------\n\n";
//      act->run(false);
//  ///////////////////////////////////////////
//      return 0;
//    } catch (SystemError &err) {
//      cerr << "(controlprocess): " << err << endl;
//    } catch (Exception &ex) {
//      cerr << "(controlprocess): " << ex << endl;
//    } catch (...) {
//      cerr << "(controlprocess): catch(...)" << endl;
//    }


#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    Panel panel;
    qmlRegisterType<Panel> ("PanelPackege", 1, 0, "Panel");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    QQmlContext *rootContext = engine.rootContext();
    rootContext->setContextProperty("panel", &panel);

    return app.exec();
}
