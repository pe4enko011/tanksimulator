import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.15
import "Components"
import "scripts/alarm.js" as Qalarm

Window {
    id: window
    width: 800
    height: 600
    visible: true
    title: qsTr("TankLevel")

    property int taerewrnk: pumpDecrementcontroller.pumpIsOn
    property int tank: panel.tankVolume
    property int pump: panel.pumpIncIsOn
    property int pumpOff: panel.pumpDecIsOn
    property int workModAvto: panel.workAuto
    property int workModHand: panel.workHand

    Item{
        id:widj
        width: 800
        height: 600

        Rectangle{
            width: 800
            height: 600
            color: "black"
        }

        MyTank {
            id: tank
        }

    }

    MyPumpSvg {
        id: pumpSvg
        x: 67
        y: 230
    }

    MyPumpoffSvg {
        id: pumpoffSvg
        x: 67
        y: 150
    }

    MyCustomButtonPumpOnPumpOff {
        id: customButtonPumpOnPumpOff
        x: 290
        y:350
        height: 80
        width: 150

        property string textButton: "Pump Inc Off"
        property int pumpOnPumpOff: -50
        property int pump: 0

        visible: workModHand ? true : false
    }

    MyCustomButtonPumpOnPumpOff {
        id: lol
        x: 100
        y:350

        property string textButton: "Pump Inc On"
        property int pumpOnPumpOff: 50
        property int pump: 1



        visible: workModHand ? true : false

    }

    MyCustomButtonPumpOnPumpOff {
        id: lpl
        x: 290
        y:400

        property string textButton: "Pump Dec Off"
        property int pumpOnPumpOff: 50
        property int pump: 2


        visible: workModHand ? true : false

    }

    MyCustomButtonPumpOnPumpOff {
        id: kjk
        x: 100
        y:400

        property string textButton: "Pump Dec On"
        property int pumpOnPumpOff: 50
        property int pump: 3


        visible: workModHand ? true : false

    }

    Label{
        id:lab
        x:160
        y:100
        text: window.tank/10
        color:"green"
    }

    Alarm {
        id: alarmSignals
        y:100
        x:300
    }

    Button{
        x: 100
        y: 350
        id: startButton
        text:"START PROCESS"
        background: Rectangle{
            width: parent.width
            height: parent.height
            color:"black"

            border.width: 3
            border.color: "white"

        }
        contentItem: Text{

            color:"white"
            text: parent.text

        }
        visible: workModAvto ? true : false
        onClicked: {
           panel.startProcess()
        }

    }

    Button{
        x: 200
        y:350

        text:"STOP PROCESS"
        anchors.left:startButton.right
        anchors.leftMargin: 50

        visible: workModAvto ? true : false

        background: Rectangle{
            width: parent.width
            height: parent.height
            color:"black"

            border.width: 3
            border.color: "white"

        }
        contentItem: Text{

            color:"white"
            text: parent.text

        }
        onClicked: {
            panel.stopProcess()

        }
    }

    Item {
        id: customSwitchModAuto
        x: 450
        y: 50

        property string switchMod: "Автоматический режим"
        property int switchTrigger1: 0
        Text{
            x:100
            id: label_type_of_work

            font.pointSize: 10
            color: "green"
            text:customSwitchModAuto.switchMod

        }
        Switch {
            id: switch1
            anchors.verticalCenter:label_type_of_work.verticalCenter
            Material.accent: Material.Green
            checked: true

            onToggled:
            {
                if(checked)
                {
                    label_type_of_work.color = "green"
                    panel.workModAutoOn()
                    panel.workModHandOff()
                    panel.stopProcess()
                     // второй свич
                    label_type_of_hand.color = "white"
                    switch2.checked = false
                }
                else
                {
                    label_type_of_work.color = "white"
                    panel.workModAutoOff()
                    switch2.checked = true



                }

            }
        }
    }

    Item {
        id: customSwitchModHand
        x: 450
        y: 100

        property string switchMod: "Ручной режим"
        property int switchTrigger1: 0
        Text{
            x:100
            id: label_type_of_hand

            font.pointSize: 10
            color: "white"
            text:customSwitchModHand.switchMod

        }
        Switch {
            id: switch2
            anchors.verticalCenter:label_type_of_work.verticalCenter
            Material.accent: Material.Green
            onToggled:
            {
                if(checked)
                {

                    label_type_of_hand.color = "green"
                    panel.workModHandOn()
                    panel.workModAutoOff()
                    panel.stopProcess()

                    // второй свич
                    label_type_of_work.color = "white"


                    switch1.checked = false


                }
                else
                {
                    label_type_of_hand.color = "white"
                    panel.workModHandOff()

                    switch1.checked = true
                }

            }
        }
    }

}



